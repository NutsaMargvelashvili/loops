let tourists =  [[], [], [], [], []];
tourists[0] = {
    name: "Mark",
    age: 19,
    tours: [
        {
            city: "Tbilisi",
            money: 120
        },
        {
            city: "London",
            money: 200
        },
        {
            city: "Rome",
            money: 150
        },
        {
            city: "Berlin",
            money: 140
        }
    ]
};
tourists[1] = {
    name: "Bob",
    age: 21,
    tours: [
        {
            city: "Miami",
            money: 90
        },
        {
            city: "Moscow",
            money: 240
        },
        {
            city: "vien",
            money: 100
        },
        {
            city: "Riga",
            money: 76
        },
        {
            city: "Kiev",
            money: 123
        }
    ]
};
tourists[2] = {
    name: "Sam",
    age: 22,
    tours: [
        {
            city: "Tbilisi",
            money: 118
        },
        {
            city: "Budapest",
            money: 95
        },
        {
            city: "Warsaw",
            money: 210
        },
        {
            city: "Vilnius",
            money: 236
        }
    ]
};
tourists[3] = {
    name: "Anna",
    age: 20,
    tours: [
        {
            city: "New-York",
            money: 100
        },
        {
            city: "Athens",
            money: 240
        },
        {
            city: "Sidney",
            money: 50
        },
        {
            city: "Tokyo",
            money: 190
        }
    ]
};
tourists[4] = {
    name: "Alex",
    age: 23,
    tours: [
        {
            city: "Paris",
            money: 96
        },
        {
            city: "Tbilisi",
            money: 134
        },
        {
            city: "Madrid",
            money: 76
        },
        {
            city: "Marseille",
            money: 210
        },
        {
            city: "Minsk",
            money: 158
        }
    ]
};

for(let i = 0; i <= 4; ++i){
    tourists[i].isAdult = (tourists[i].age >= 21) ? (tourists[i].isAdult = true, console.log(`${tourists[i].name} is adult.`)) : ( tourists[i].isAdult = false, console.log(`${tourists[i].name} is not adult.`));
}

console.log("");

for(let i = 0; i <= 4; ++i){
    tourists[i].beenInGeorgia = "hasn't been in Georgia";
    for(let j = 0; j < tourists[i].tours.length; ++j){
        if(tourists[i].tours[j].city == "Tbilisi"){
            tourists[i].beenInGeorgia = "has been in Georgia";
        }
    }
    console.log(`${tourists[i].name} ${tourists[i].beenInGeorgia}.`);
}

console.log("");

for(let i = 0; i <= 4; ++i){
    tourists[i].sumOfMoney = 0;
    for(let j = 0; j < tourists[i].tours.length; ++j){
        tourists[i].sumOfMoney += tourists[i].tours[j].money;
    }
    console.log(`${tourists[i].name}'s spent ${tourists[i].sumOfMoney}$.`);
}

console.log("");

for(let i = 0; i <= 4; ++i){
    tourists[i].averageOfMoney = tourists[i].sumOfMoney / tourists[i].tours.length;
    console.log(`${tourists[i].name}'s spent on average ${tourists[i].averageOfMoney}$.`);
}

console.log("");

for(let i = 0; i <= 3; ++i){
    if(tourists[i].sumOfMoney < tourists[i + 1].sumOfMoney){
        bestSpent = tourists[i + 1];
    }       
}
console.log(`${bestSpent.name}'s spent the highest amount of money ---> ${bestSpent.sumOfMoney}$.`);
// console.log(tourists);