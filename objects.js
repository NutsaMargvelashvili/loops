let studentsArr = [[], [], [], [], []];

studentsArr[0] = {
    firstName: "Jean",
    lastName: "Reno",
    age: 26,
    subjects: {
        javascript: 62,
        react: 57,
        python: 88,
        java: 90
    }
};

studentsArr[1] = {
    firstName: "Claude",
    lastName: "Monet",
    age: 19,
    subjects: {
        javascript: 77,
        react: 52,
        python: 92,
        java: 67
    }
};

studentsArr[2] = {
    firstName: "Van",
    lastName: "Gogh",
    age: 21,
    subjects: {
        javascript: 51,
        react: 98,
        python: 65,
        java: 70
    }
};
studentsArr[3] = {
    firstName: "Dam",
    lastName: "Square",
    age: 36,
    subjects: {
        javascript: 82,
        react: 53,
        python: 80,
        java: 65
    }
};
let credits = {
    javascript: 4,
    react: 7,
    python: 6,
    java: 3
}; 
/************************************/

console.log("The sum of the scores:");

for(let i = 0; i <= 3; ++i){
  studentsArr[i].sum =
  studentsArr[i].subjects.javascript +
  studentsArr[i].subjects.react +
  studentsArr[i].subjects.python +
  studentsArr[i].subjects.java;
}

console.log(
  `    ${studentsArr[0].firstName} ${studentsArr[0].lastName}    --->  ${studentsArr[0].sum}`
);

console.log(
  `    ${studentsArr[1].firstName} ${studentsArr[1].lastName} --->  ${studentsArr[1].sum}`
);

console.log(
  `    ${studentsArr[2].firstName} ${studentsArr[2].lastName}     --->  ${studentsArr[2].sum}`
);

console.log(
  `    ${studentsArr[3].firstName} ${studentsArr[3].lastName}   --->  ${studentsArr[3].sum}`
);

// /******************************************/

console.log("");
console.log("The average of the scores:");
for(let i = 0; i <= 3; ++i){
  studentsArr[i].average =
  (studentsArr[i].subjects.javascript *
    credits.javascript +
    studentsArr[i].subjects.react *
      credits.react +
    studentsArr[i].subjects.python *
      credits.python +
    studentsArr[i].subjects.java *
      credits.java) /
  (credits.javascript +
    credits.react +
    credits.python +
    credits.java);
}

console.log(
  `    ${studentsArr[0].firstName} ${studentsArr[0].lastName}    --->  ${studentsArr[0].average}`
);

console.log(
  `    ${studentsArr[1].firstName} ${studentsArr[1].lastName} --->  ${studentsArr[1].average}`
);

console.log(
  `    ${studentsArr[2].firstName} ${studentsArr[2].lastName}     --->  ${studentsArr[2].average}`
);

console.log(
  `    ${studentsArr[3].firstName} ${studentsArr[3].lastName}   --->  ${studentsArr[3].average}`
);

// // /**************************************/

console.log("");
console.log("GPA:");
let i = 0;
while( i <= 3){
  studentsArr[i].gpa =
  studentsArr[i].average >= 91
    ? 4
    : studentsArr[i].average >= 81
    ? 3
    : studentsArr[i].average >= 71
    ? 2
    : studentsArr[i].average >= 61
    ? 1
    : 0.5;
    ++i;
}

console.log(
  `    ${studentsArr[0].firstName} ${studentsArr[0].lastName}    --->  ${studentsArr[0].gpa}`
);
console.log(
  `    ${studentsArr[1].firstName} ${studentsArr[1].lastName} --->  ${studentsArr[1].gpa}`
);
console.log(
  `    ${studentsArr[2].firstName} ${studentsArr[2].lastName}     --->  ${studentsArr[2].gpa}`
);
console.log(
  `    ${studentsArr[3].firstName} ${studentsArr[3].lastName}   --->  ${studentsArr[3].gpa}`
);

// /********************************************************/

console.log("");
console.log("Awards Ceremony:");

let avgScore =
  (studentsArr[0].average +
    studentsArr[1].average +
    studentsArr[2].average +
    studentsArr[3].average) /
  4;
  i = 0;
  while( i <= 3){
    console.log(
      studentsArr[i].average >= avgScore
        ? `    Congratulations, ${studentsArr[i].firstName}!!! You've got a red diploma.`
        : `    Opps :( ${studentsArr[i].firstName}, try harder next time.`
    );
    ++i;
  }

// /*******************************************************************/

let highestGpa = [];

highestGpa = studentsArr[0];

console.log("");
console.log("The students who have the highest GPA:");
console.log("");

for(let i = 1; i <= 3; ++i){
  if (highestGpa.gpa < studentsArr[i].gpa) {
    highestGpa = studentsArr[i];
  }
}

console.log("First Name     Last Name     GPA");

if (highestGpa.gpa == studentsArr[0].gpa) {
  console.log(
    `  ${studentsArr[0].firstName}           ${studentsArr[0].lastName}         ${studentsArr[0].gpa}`
  );
}
if (highestGpa.gpa == studentsArr[1].gpa) {
  console.log(
    `  ${studentsArr[1].firstName}         ${studentsArr[1].lastName}        ${studentsArr[1].gpa}`
  );
}
if (highestGpa.gpa == studentsArr[2].gpa) {
  console.log(
    `  ${studentsArr[2].firstName}            ${studentsArr[2].lastName}         ${studentsArr[2].gpa}`
  );
}
if (highestGpa.gpa == studentsArr[3].gpa) {
  console.log(
    `  ${studentsArr[3].firstName}            ${studentsArr[3].lastName}       ${studentsArr[3].gpa}`
  );
}

// /***********************************************/

let bestAdult = [];

bestAdult = studentsArr[0];

console.log("");
console.log("The best student above 21:");
console.log("");

for(let i = 1; i <= 3; ++i){
  if (
    studentsArr[1].age > 21 &&
    bestAdult.average < studentsArr[1].average
  ) {
    bestAdult = studentsArr[1];
  }
}
console.log("First Name     Last Name     Age     Average Score");
console.log(
  `  ${bestAdult.firstName}           ${bestAdult.lastName}        ${bestAdult.age}          ${bestAdult.average}`
);

// /************************************************/

console.log("");
console.log("The best front-end student:");
console.log("");

let bestFrontEnd = [];

bestFrontEnd = studentsArr[0];
bestFrontEnd.score =
  (studentsArr[0].subjects.javascript +
    studentsArr[0].subjects.react) /
  2;

  for(let i = 1; i <= 3; ++i){
    if (
      bestFrontEnd.score <
      (studentsArr[i].subjects.javascript +
        studentsArr[i].subjects.react) /
        2
    ) {
      bestFrontEnd = studentsArr[i];
      bestFrontEnd.score =
        (studentsArr[i].subjects.javascript +
          studentsArr[i].subjects.react) /
        2;
    }
  }

console.log("First Name     Last Name     Score (Js + React)");
console.log(
  `  ${bestFrontEnd.firstName}            ${bestFrontEnd.lastName}               ${bestFrontEnd.score}`
);
